({
	packItem : function(component, event, helper) {
		
        component.set("v.item.packed__c",true);
        
        var isDis = event.getSource();
        isDis.set("v.disabled",true);
    }
})