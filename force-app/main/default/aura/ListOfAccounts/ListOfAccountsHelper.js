({
	getAccontRecords : function(component) {
		var action = component.get("c.getAccountRecords"); 
        action.setCallback(this, function(response) {
            var state = response.getState();             
            if (component.isValid() && state === "SUCCESS")
                component.set("v.accLst", response.getReturnValue());   
        });
        $A.enqueueAction(action);
	},
    makeCallOut : function(component) {
          var action = component.get("c.getCalloutResponse"); 
        action.setParams({
            "url": 'http://www.timeapi.org/pdt/next+monday'
        });
     action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {     
                component.set("v.response", response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
	}
})