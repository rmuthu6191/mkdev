({
    doInit : function(component, event, helper) {
        var action = component.get("c.getOpptyList");
		console.log('opptyLst-->');
        action.setCallback(this,function(response){
			console.log('sucess->'+ response.getState());         
            if(response.getState() === "SUCCESS"){
                var opptyLst = response.getReturnValue();
                console.log('opptyLst-->'+opptyLst);
                component.set('v.wrapperOppty',opptyLst);
            }
        });

        $A.enqueueAction(action);

    }
})