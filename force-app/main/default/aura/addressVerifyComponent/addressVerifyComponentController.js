({
    doInit : function(component, event, helper) {
        var recordID = component.get("v.recordId");
        console.log(recordID);
        alert(recordID);
        
        var action = component.get("c.verifyAddress");
        action.setParams({'AccountID' : recordID});

        action.setCallback(this,function (response) {
            if(response.getState()==="SUCCESS"){
                alert(response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    }
})