({
	eventCall : function(component, event, helper) {
		var evt =  $A.get("e.c:TestEvent");
        var inputName = component.get("v.inputName");
        evt.setParams({"Name": inputName});
        evt.fire();
	}
})