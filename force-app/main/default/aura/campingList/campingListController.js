(
    {
    doInit : function(component,event,helper){
		var action = component.get("c.getItems");
        
        action.setCallback(this, function(response){
            if(response.getState == "SUCCESS"){
                component.set("v.items",response.getREturnValue());
            }
            else{
                console.log("Failed with State"+State);
            }
            
        });
        
        $A.enqueueAction(action);
    },
    
        handleAddItem: function(component, event, helper) {
    var addItm = event.getParam("newItem");
    helper.createItem(component, addItm);

},

    createItem: function(component, newItem) {
    var action = component.get("c.saveItem");
    action.setParams({
        "newitem": newItem
    });
    action.setCallback(this, function(response){
        var state = response.getState();
        if (component.isValid() && state === "SUCCESS") {
            var items = component.get("v.items");
            items.push(response.getReturnValue());
            component.set("v.items", items);
        }
    });
    $A.enqueueAction(action);
},
    
})