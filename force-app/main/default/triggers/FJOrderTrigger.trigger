/**
 * @description       : 
 * @author            : kumarmuthu9@gmail.com
 * @group             : 
 * @last modified on  : 07-28-2020
 * @last modified by  : kumarmuthu9@gmail.com
 * Modifications Log 
 * Ver   Date         Author                  Modification
 * 1.0   07-28-2020   kumarmuthu9@gmail.com   Initial Version
**/
trigger FJOrderTrigger on Fj_Product__c (before insert) {
    Set<Id> ProductId = new Set<Id>();
    for (Fj_Product__c fj : Trigger.new){
        ProductId.add(fj.id);
    }
}