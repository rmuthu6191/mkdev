trigger DuplicateAcc on Account (before insert,before update) {
    Set<String> name = new Set<String>();
    
    for(Account a : Trigger.new){
        name.add(a.Name);        
    }
    List<Account> DuplicateAccList = [select id,name from account where name =: name];
    Set<String> DuplicateAccName = new Set<String>();
    for(Account a : DuplicateAccList){
        DuplicateAccName.add(a.Name);
    }
    for(Account a : Trigger.new){
        if(a.Name!= null){
            if(DuplicateAccName.contains(a.Name)){
                a.name.addError('Record already exit with same name');
            }
        }
    }
}