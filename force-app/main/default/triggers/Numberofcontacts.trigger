/**
 * @File Name          : Numberofcontacts.trigger
 * @Description        : 
 * @Author             : kumarmuthu9@gmail.com
 * @Group              : 
 * @Last Modified By   : kumarmuthu9@gmail.com
 * @Last Modified On   : 1/7/2020, 3:58:44 pm
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    1/7/2020   kumarmuthu9@gmail.com     Initial Version
**/
trigger Numberofcontacts on Contact (after insert,after update, after delete, after undelete) {
    
    List<Account> Accountlist = new List<Account>();
    List<id> Accountidlst = new List<id>();
   	List<Account> UpdateAccRecords = new List<Account>();
    if(Trigger.isinsert||Trigger.isupdate || Trigger.isUndelete){
    for(contact con:Trigger.new)
    {        
        Accountidlst.add(con.AccountId);        
    }
    }
    if(Trigger.isdelete){
    for(contact con:Trigger.old)
    {        
        Accountidlst.add(con.AccountId);        
    }
    }
   if(Accountidlst.size()>0)
    {
        Accountlist =[select id,Number_of_Related_Contacts__c,(select id from contacts) from Account where id IN: Accountidlst];
       for(Account acc:Accountlist)      
        {          
          acc.Number_of_Related_Contacts__c = acc.contacts.size();        
         	UpdateAccRecords.add(acc);		
        }
        
        if(UpdateAccRecords.size()>0)
        {
            update UpdateAccRecords;
        }
  }
}