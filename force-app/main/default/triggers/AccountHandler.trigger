trigger AccountHandler on Account (before insert, after insert) {
    
    if(Trigger.isInsert)
    {
    if(Trigger.isBefore)
    {
    For (Account a : Trigger.new)
    {
        a.Description ='New Test Account';
    }
    }
    else if(Trigger.isAfter)
    {
        List<Task> TaskLst = new List<Task>();
        for(Account Acc: Trigger.new)
        {
            Task t = new Task();
            t.priority ='high';
            t.subject='Test Task';
            t.whatid=Acc.id;
            TaskLst.add(t);     
        }
        insert TaskLst;
    }
    }
}