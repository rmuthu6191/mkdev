trigger AccountAddressTrigger on Account (after insert, after update) {
List<Account> acc = new List<Account>();
    List<Account> accQuery = [select id, BillingPostalCode,Description, ShippingPostalCode from Account where id IN: Trigger.new];
    for(Account a : accQuery){
        If ( a.BillingPostalCode!=Null) {
            a.ShippingPostalCode = a.BillingPostalCode;
            a.Description ='Address updated';
            acc.add(a);
        }   
    }
    
    update acc;
}