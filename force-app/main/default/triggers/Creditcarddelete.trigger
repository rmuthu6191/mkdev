trigger Creditcarddelete on Credit_Card__c (before delete) {
List<Credit_Card_Application__c> tobedeleted = [select id from Credit_Card_Application__c where Credit_Card__c in: Trigger.old];
    delete tobedeleted;
}