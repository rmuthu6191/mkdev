trigger AccountDeletion on Account (before delete) {


    Map<Id, Account> oldCopy = Trigger.oldMap.clone();

    oldCopy.keySet().retainAll(
        new Map<Id, AggregateResult>(
            [SELECT AccountId Id 
             FROM Contact 
             WHERE AccountId = :Trigger.old 
             GROUP BY AccountId HAVING Count(Id)>5]).keySet());

    for(Account record: oldCopy.values()) {
        record.addError('You cannot delete an account with contacts.');
    }

    
    /*for (Account a : [select ID from Account where id in (select AccountId from Opportunity) and id in : Trigger.old ]){
        Trigger.oldMap.get(a.id).addError('Can\'t delete account with related opportunity');
    }*/

}