/**
 * @File Name          : updateClodedAccUser.trigger
 * @Description        : 
 * @Author             : kumarmuthu9@gmail.com
 * @Group              : 
 * @Last Modified By   : kumarmuthu9@gmail.com
 * @Last Modified On   : 07-07-2020
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    7/7/2020   kumarmuthu9@gmail.com     Initial Version
**/
trigger updateClodedAccUser on Account (Before update) {
    Map<id,List<Account>> bookingOwnerMap = new Map<Id,List<Account>>();
    
    
    for(Account Acc : Trigger.new){
        if(Acc.Account_Status__c =='Closed' && Trigger.oldMap.get(Acc.id).Account_Status__c != 'Closed'){
            if(!bookingOwnerMap.keyset().contains(Acc.OwnerId)){
            bookingOwnerMap.put(Acc.OwnerId,new List<Account>());    
            }
            bookingOwnerMap.get(Acc.OwnerId).add(Acc);
        }        
    }

    List<user> userRec = [SELECT id,Number_of_closed_accounts__c from user where id in : bookingOwnerMap.keyset()];
    
    List<user> updateUser = new List<User>();

    for(User u : userRec){
        
        Integer closedAcc = bookingOwnerMap.get(u.id).size();
        
        u.Number_of_closed_accounts__c = u.Number_of_closed_accounts__c + closedAcc  ;
        System.debug('closed acc count -->'+ u.Number_of_closed_accounts__c);
        updateUser.add(u);
    }
    if(updateUser.size()>0){
        Database.update(updateUser, false);
    }
}