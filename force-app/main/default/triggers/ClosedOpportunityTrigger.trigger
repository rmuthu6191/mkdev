trigger ClosedOpportunityTrigger on Opportunity (after insert, after update) {
    
    List<Opportunity> OpptyList = [select id,stageName from Opportunity where stageName ='Closed Won' and ID IN : Trigger.New];
	List<Task> InsertTask = new List<Task> ();
    
    for (Opportunity Opp : OpptyList){
        Task a = new Task();
        a.WhatId = Opp.id;
        a.Subject = 'Follow Up Test Task';
        InsertTask.add(a);
    }
    
    if(InsertTask.size()>0){
        insert InsertTask;
    }
}