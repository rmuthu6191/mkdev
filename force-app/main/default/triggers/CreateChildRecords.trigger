/**
 * @description       : 
 * @author            : kumarmuthu9@gmail.com
 * @group             : 
 * @last modified on  : 08-08-2020
 * @last modified by  : kumarmuthu9@gmail.com
 * Modifications Log 
 * Ver   Date         Author                  Modification
 * 1.0   08-08-2020   kumarmuthu9@gmail.com   Initial Version
**/
trigger CreateChildRecords on ParentObject__c (after insert) {
    List<ChildObject__c> childObj = new List<ChildObject__c>();
    for(ParentObject__c PO : Trigger.new){
        for(Integer i = 1; i<=2; i++){
            ChildObject__c CO = new ChildObject__c();
            CO.ParentObject__c = PO.id;
            CO.Name = 'Test Rec ' + i;
            childObj.add(CO);
        }
    }
    if(childObj.size()>0){
        Database.insert(childObj, false);
    }
}