/**
 * @description       : 
 * @author            : kumarmuthu9@gmail.com
 * @group             : 
 * @last modified on  : 08-09-2020
 * @last modified by  : kumarmuthu9@gmail.com
 * Modifications Log 
 * Ver   Date         Author                  Modification
 * 1.0   08-09-2020   kumarmuthu9@gmail.com   Initial Version
**/
trigger RollUpSummaryLookup on ChildObject__c (after insert, after update, after delete, after undelete) {

    set<Id> parentRecIds = new Set<Id>();
    List<ParentObject__c> parentRecUpdateLst = new List<ParentObject__c>();

    if(Trigger.isInsert || Trigger.isUpdate || Trigger.isUndelete){
        for (ChildObject__c CO : Trigger.new) {
            if (CO.ParentObject__c != null) {
                parentRecIds.add(CO.ParentObject__c);
            }          
        }
    }

    if (Trigger.isDelete || Trigger.isUpdate) {
        for (ChildObject__c CO : Trigger.old) {
            if (CO.ParentObject__c != null) {
                parentRecIds.add(CO.ParentObject__c);
            }
        }
    }

    Map<id,ParentObject__c> parentMap = new Map<id,ParentObject__c>([select id, NumberOfChilds__c from ParentObject__c where id IN :parentRecIds]);

    for (ParentObject__c PO : [select Id, Name, NumberOfChilds__c,(select id from ChildObjects__r) from ParentObject__c where Id IN :parentRecIds]) {
        parentMap.get(PO.id).NumberOfChilds__c = PO.ChildObjects__r.size();
        parentRecUpdateLst.add(parentMap.get(PO.id));
    }

    if (parentRecUpdateLst.size()>0) {
        Database.update(parentRecUpdateLst, false);
    }

}