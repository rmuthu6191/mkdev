@isTest
private class TemperatureConverterTest {
    
    @isTest static void testwarmTemp(){
        Decimal wT = TemperatureConverter.FahrenheitToCelsius(70);
		system.assertEquals(21.11, wT);
    }

    @isTest static void testFreezingPoint(){
        Decimal FT = TemperatureConverter.FahrenheitToCelsius(32);
		system.assertEquals(0, FT);
    }
    
    @isTest static void testBoilingPoint(){
        Decimal BT = TemperatureConverter.FahrenheitToCelsius(212);
		system.assertEquals(100, BT,'Boilingpoint temperature is not expected');
    }
    
    @isTest static void testNegativeTemp(){
        Decimal NT = TemperatureConverter.FahrenheitToCelsius(-10);
		system.assertEquals(-23.33, NT);
    }
}