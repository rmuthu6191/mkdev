/**
 * @File Name          : ForexUpdateUtilClass.cls
 * @Description        : 
 * @Author             : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Group              : 
 * @Last Modified By   : kumarmuthu9@gmail.com
 * @Last Modified On   : 20/5/2020, 1:23:47 pm
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    7/5/2020   ChangeMeIn@UserSettingsUnder.SFDoc     Initial Version
**/
public class ForexUpdateUtilClass {
    
    @future(callout=true)
    public static void FutureMethod(){
        http h = new http();
        httpRequest req = new httpRequest();
        // Retrieve base currency from custom settings.  Custom settings used to avoid hardcoding and simplify migration from Test to PRD
        Base_Currency__c BC = Base_Currency__c.getvalues('Current'); 
        String apiEndpoint = 'https://api.exchangeratesapi.io/latest?base=';
        String BaseCurrency = BC.Currency_Code__c;
        String endPoint = apiEndpoint + BaseCurrency;
        req.setEndpoint(endPoint);
        req.setMethod('GET');
        
        httpResponse res = h.send(req);
        if(res.getStatusCode() == 200) {
            Map<String, Object> results = (Map<String, Object>) JSON.deserializeUntyped(res.getBody());
            system.debug('results -->'+ results);
            system.debug('rates-->'+ results.get('rates'));
            String JsonString = JSON.serialize(results.get('rates'));    
            Map<String,Object> rateMap =  (Map<String,Object>) JSON.deserializeUntyped(JsonString);            
            List<CurrencyType> CurrencyList = [select id,ConversionRate,DecimalPlaces,IsoCode,IsActive from CurrencyType];            
            for(CurrencyType CT : CurrencyList){
                if(rateMap.containsKey(CT.IsoCode) && CT.IsActive == True){                    
                    Http ht = new Http();            
                    HttpRequest reqt = new HttpRequest();                        
                    reqt.setEndpoint(URL.getSalesforceBaseUrl().toExternalForm() + '/services/data/v28.0/sobjects/CurrencyType/');                        
                    reqt.setMethod('POST'); 
                    String ReqBody;
                    ReqBody =  '{ "IsoCode" : "' + CT.IsoCode + '","DecimalPlaces" : '+ CT.DecimalPlaces +', "ConversionRate" :' + (Decimal)rateMap.get(CT.IsoCode) + ', "IsActive" : "true" }';
                    reqt.setBody(ReqBody);
                    system.debug('reqbody-->'+ReqBody);
                    reqt.setHeader('Authorization', 'OAuth ' + UserInfo.getSessionId());                        
                    reqt.setHeader('Content-Type', 'application/json');          
                    HttpResponse resp = ht.send(reqt);
                    
                }
            }                       
        }
        
    }
}