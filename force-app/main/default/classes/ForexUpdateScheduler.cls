/**
 * @File Name          : ForexUpdateScheduler.cls
 * @Description        : 
 * @Author             : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Group              : 
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 7/5/2020, 10:41:04 am
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    7/5/2020   ChangeMeIn@UserSettingsUnder.SFDoc     Initial Version
**/
global class ForexUpdateScheduler implements Schedulable {
	
    global void execute(SchedulableContext sc){
        ForexUpdateUtilClass.FutureMethod();
    }
}