/**
 * @description       : 
 * @author            : kumarmuthu9@gmail.com
 * @group             : 
 * @last modified on  : 07-28-2020
 * @last modified by  : kumarmuthu9@gmail.com
 * Modifications Log 
 * Ver   Date         Author                  Modification
 * 1.0   07-28-2020   kumarmuthu9@gmail.com   Initial Version
**/
public with sharing class opptyList {
    public opptyList() {

    }

    @AuraEnabled
    public static List<opptyList.wrapperClassOppty> getOpptyList(){
        set<id> opptyId = new set<id>();

        for(Opportunity op : [select id, name from Opportunity limit 100]){
            opptyId.add(op.id);
        }

        List<Task> taskList = [select id,WhatId,what.name,Subject from Task  where WhatId in : opptyId LIMIT 100];

        List<opptyList.wrapperClassOppty> wrClassLst = new  List<opptyList.wrapperClassOppty>();
        
        for(Task taskRecord : taskList){
            opptyList.wrapperClassOppty wrClassRec = new opptyList.wrapperClassOppty();
            wrClassRec.opptyName = taskRecord.what.name;
            wrClassRec.OpptyId = taskRecord.whatId;
            wrClassRec.Taskid = taskRecord.id;
            wrClassRec.taskSubject = taskRecord.Subject;
            wrClassLst.add(wrClassRec);
        }
        
        system.debug('wrclassCount-->'+ wrClassLst.size());
		system.debug('wrclasslst-->'+ wrClassLst);
        return wrClassLst;
    }

    public class wrapperClassOppty{
  		@AuraEnabled public String opptyName{get;set;}
        @AuraEnabled public String OpptyId{get;set;}
        @AuraEnabled public String Taskid{get;set;}
        @AuraEnabled public String taskSubject{get;set;}
    }

}