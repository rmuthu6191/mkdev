public with sharing class PackageController {
    
    public List<WrapperClass> listWrapper {get;set;}
    public Boolean saveMethod {get;set;}
    public Boolean Listtabel {get;set;}
    public Package__C pkgnew {get;set;}
    public List<Package__c> editPkg {get;set;}
    public Boolean allBool {get;set;}
    
    public PageReference InsertNew(){
        if (Schema.sObjectType.Package__c.isCreateable()){
            try{
                insert pkgnew;
                reloadWrapper();  
                pkgnew= null;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'Package record created successfully')); 
            }
            catch(System.DmlException e){
                ApexPages.addMessages(e);
            }
        }else{
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'You don\'t have access to create package record')); 
        }
        return null;
    }
    
    
    public PackageController() {
        saveMethod = false;
        Listtabel = true;
        pkgnew = new Package__c();
        editPkg = new List<Package__c>();
        listWrapper = new List<WrapperClass>();
        for(Package__c pkg : [Select id, name, Latest_Version__c,Namespace__c from Package__c]) {
            listWrapper.add(new WrapperClass(pkg));
        }
    }
    
    public void loaddata(){
        List<WrapperJson> WrapperList = new List<WrapperJson>();
        HttpRequest req = new HttpRequest();
        HttpResponse res = new HttpResponse();
        Http http = new Http();
        req.setEndpoint('https://s3.amazonaws.com/targetx-sfdc-interview/packages.json');        
        req.setMethod('GET'); 
        try{
        res = http.send(req);
        }
        catch(System.calloutException e){ 
         ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Unable to load Data. An Exception '+e+' occured.Please reachout to support team')); 
        }
        if(res.getstatusCode() == 200 && res.getbody() != null)     {
            
            String response =   res.getbody().replace('"Namespace__c":', '"Namespace":');
            String response1 = response.replace('"Latest_Version__c":', '"LatestVersion":');
            WrapperList=(List<WrapperJson>)json.deserialize(response1,List<WrapperJson>.class);
        }
        List<Package__c> newpkg = new List<Package__c>();
        if(WrapperList.size()>0){
            for(WrapperJson w : WrapperList){
                Package__c pkg = new Package__C();
                pkg.Name = w.Name; 
                pkg.Namespace__c = w.Namespace;
                pkg.Latest_Version__c =String.valueOf(w.LatestVersion);
                newpkg.add(pkg);
            }
        }
        Schema.SObjectField f = Package__c.Fields.Namespace__c;
        if(newpkg.size()>0)
            if(Schema.sObjectType.Package__c.isCreateable() && Schema.sObjectType.Package__c.isUpdateable()){
                try{
                    Database.upsert(newpkg, f);
                    reloadWrapper();
                     ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'Package records loaded successfully')); 
                }  
            catch(System.DmlException e){
                ApexPages.addMessages(e);
            }
          }else{
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'You don\'t have access to upsert package record')); 
        }
    }
    
    public class WrapperJson {
        public String Name;
        public String Namespace;
        public Double LatestVersion;
    }
    
    public void reloadWrapper(){
        listWrapper.clear();
        for(Package__c pkg : [Select id, name, Latest_Version__c,Namespace__c from Package__c]) {
            listWrapper.add(new WrapperClass(pkg));
        }
    }
    
    public void save() {
        saveMethod = false;
        Listtabel = true;
        if(editPkg.size()>0)
           if(Schema.sObjectType.Package__c.isUpdateable()){
               try{
                    Database.update(editPkg);
                    editPkg.clear();
                    reloadWrapper();
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'Package records updated successfully')); 
               }  catch(System.DmlException e){
                ApexPages.addMessages(e);
            }
           }else{
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'You don\'t have access to update package record')); 
        }
    }
    public class WrapperClass {
        public Boolean checked {get;set;}
        public Package__c pkg {get;set;}
        public WrapperClass(Package__c pkg) {
            this.pkg = pkg;
            checked = false;
        }
    }
    
    
    
    public void editAction(){
        if(listWrapper.size()>0){
            Integer count = 0;
            for(WrapperClass wrap: listWrapper){
                if(wrap.checked == true){
                    count ++;
                    editPkg.add(wrap.pkg);
                }
            }
            if(count>0){
                saveMethod = true;
                Listtabel = false;
            }
            else 
            {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Select atleast one package record to edit'));
            }
            
        }
    }
    
    public PageReference del() {
        List<Package__c> listPkgForDel = new List<Package__c>();
        for(WrapperClass w : listWrapper) {
            if(w.checked) {
                listPkgForDel.add(w.pkg);
            } 
        }
        if(listPkgForDel.size() > 0) {
            if (Schema.sObjectType.Package__c.isDeletable()){
                try{
                    delete listPkgForDel;
                    reloadWrapper();
                     ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'Package records deleted successfully')); 
                }
                catch(System.DmlException e){
                    ApexPages.addMessages(e);
                }
            }else{
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'You don\'t have access to delete package record')); 
            }
            
        } else {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Select atleast one package record to delete'));
        }
        return null;
    }
    
    public void selectAll() {
        
        if(allBool) {
            for(WrapperClass w : listWrapper) {
                w.checked = true;
            }
        } else {
            for(WrapperClass w : listWrapper) {
                w.checked = false;
            }
        }
    }
    
    
}