public class MPEmpowerController {
    public ApexPages.StandardController controller;
    public String AppID {get;set;}
    
    Empower_Application__c EmpwrApp = new Empower_Application__c();
    
    public MPEmpowerController(ApexPages.StandardController controller) {
    this.controller = controller;
    EmpwrApp = (Empower_Application__c)controller.getRecord();
    }    
    
    public Empower_Application__c getMPE(){
    
    AppID = ApexPages.currentPage().getParameters().get('id');
    
    if(EmpwrApp.Id == null && AppID != null){
            EmpwrApp = [Select Id, Region_Requested__c,Referring_Distributor__c,Relationship_Type__c,Technology_Segment_Interest__c,Applicant_Salutation__c,Applicant_Title__c,Applicant_First_Name__c,Applicant_Phone_Number__c,Applicant_Last_Name__c,Applicant_Mobile_Number__c,Applicant_Email_Address__c,Preferred_Language__c from Empower_Application__c where id =:AppID]; 
            
    }
    return EmpwrApp;
    }
    
    
    public PageReference nextPage2(){    
    return page.MPEPage2;    
    }
    
    public PageReference prevPage1(){
    return page.MPEPage1;    
    }
    
    public PageReference save(){
    EmpwrApp.Status__c ='New';
    upsert EmpwrApp;
    return null;
    
    }
}