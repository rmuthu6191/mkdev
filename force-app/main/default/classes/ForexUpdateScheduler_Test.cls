/**
 * @File Name          : ForexUpdateScheduler_Test.cls
 * @Description        : 
 * @Author             : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Group              : 
 * @Last Modified By   : kumarmuthu9@gmail.com
 * @Last Modified On   : 17/6/2020, 11:01:06 am
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    18/5/2020   ChangeMeIn@UserSettingsUnder.SFDoc     Initial Version
**/
@isTest
private  class ForexUpdateScheduler_Test {   
    @TestSetup
    static void makeData(){
        Base_Currency__c BC = new Base_Currency__c();
        BC.Name = 'Current';
        BC.Currency_Code__c = 'JPY';
        insert BC;
    }

    static testMethod void myTestMethod() {                
       
        Test.startTest();      
        Test.setMock( HttpCalloutMock.class, new HTTPMockCallout_Forex() );
        DateTime now  = DateTime.now();
        DateTime nextRunTime = now.addMinutes(1);
        String cronString = '' + nextRunTime.second() + ' ' + nextRunTime.minute() + ' ' + 
            nextRunTime.hour() + ' ' + nextRunTime.day() + ' ' + 
            nextRunTime.month() + ' ? ' + nextRunTime.year(); 
        System.schedule(ForexUpdateScheduler.class.getName() + '-' + now.format(), cronString, new ForexUpdateScheduler());
        Test.stopTest();
   }
}