public class OrderRollUpClass {      
@future 
    public static void RollupOrder(List<ID> acclist){
        system.debug('inside future method');
    List<Account> Acclst = [select id,name,Order_Count__c, (select Id from Orders__r) from account where id in : acclist]; 
    List<Account> AccUpdate = new List<Account> ();
    for(Account acc : Acclst){
        system.debug('oder size-->'+acc.orders__r.size());
        acc.Order_Count__c = acc.orders__r.size();
        AccUpdate.add(acc);
    }
    try{
        if(AccUpdate.size()>0)
            Database.update(AccUpdate, false);
        system.debug('Acc updated');
    }
    Catch(System.DmlException e){
	system.debug('Exception msg ->'+ e.getMessage());    
    }
}
}