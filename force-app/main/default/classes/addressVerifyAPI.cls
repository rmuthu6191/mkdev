/**
 * @File Name          : addressVerifyAPI.cls
 * @Description        : 
 * @Author             : kumarmuthu9@gmail.com
 * @Group              : 
 * @Last Modified By   : kumarmuthu9@gmail.com
 * @Last Modified On   : 3/7/2020, 10:36:22 am
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    1/7/2020   kumarmuthu9@gmail.com     Initial Version
**/
public with sharing class addressVerifyAPI {
  
    @AuraEnabled
    public static string verifyAddress(ID AccountID){
        Account Acc = [select id,BillingCity, BillingStreet,BillingState,Address_Verified__c from Account where id =: AccountID LIMIT 1];
        String message;
        String BillingStreet = EncodingUtil.urlEncode(Acc.BillingStreet, 'UTF-8').replace('+', '%20') ;
        String BillingCity = EncodingUtil.urlEncode(Acc.BillingCity, 'UTF-8').replace('+', '%20') ;
        String BillingState = EncodingUtil.urlEncode(Acc.BillingState, 'UTF-8').replace('+', '%20') ;
        String endPointUrl = 'https://us-street.api.smartystreets.com/street-address?auth-id=15acdea8-e66f-4683-c2d5-47edb141f931&auth-token=YXrC2EK2UaO3K7I0M3Ey&';
        endPointUrl += 'street=' + BillingStreet;
        endPointUrl += '&city=' + BillingCity;
        endPointUrl += '&state=' + BillingState;
        
      // String endPointWithEncode = EncodingUtil.urlEncode(endPointUrl, 'UTF-8').replace('+', '%20') ;
		system.debug('endpoint url-->'+ endPointUrl);

        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setMethod('GET');
        req.setEndpoint(endPointUrl);
        HttpResponse res = h.send(req);
        if(res.getStatusCode() == 200 && res.getBody() != null){
            Acc.Address_Verified__c = true;
            update Acc;
            message = 'Address successfully verified';
            return message;
        } else {
            if(Acc.Address_Verified__c){
            Acc.Address_Verified__c = false;
            update Acc;    
        }            
            message ='Address is invalid';
            return message;
        }
    }
  
}