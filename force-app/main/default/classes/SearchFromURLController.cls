public class SearchFromURLController {
    Public String name {get; set;}
    Public Boolean Searched {get; set;}
    Public List<Account> accounts {get; set;}
    
    Public SearchFromURLController(){
        Searched =false;
        String namestr = ApexPages.currentPage().getParameters().get('name');
        if(namestr!=null)       
        {
            name=namestr;
            executeSearch();
        }                
    }
    
    Public PageReference executeSearch(){
        Searched = true;
        String SearchQuery = '%' + name + '%';
        accounts = [select name,Id,Industry, Type from Account where Name LIKE : SearchQuery];
        return null;
    }    
    
}