public class AddPrimaryContact implements Queueable  {

    public contact con;
    public String State;
    
    public AddPrimaryContact(Contact con, String State){
        this.con = con;
        this.state = State;
    }
    
    public void execute(QueueableContext qc){

        List<Account> Acc = [select id from account where BillingState = :State LIMIT 200 ];
        
        List<Contact> conlist = new List<Contact>();
        
        for(Account a : Acc){
            
            Contact ConInst = con.clone(false,false,false,false);
            ConInst.AccountId = a.id;
            
            conlist.add(ConInst);
        }
        
        insert conlist;
    }
}