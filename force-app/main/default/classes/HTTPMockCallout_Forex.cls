/**
 * @File Name          : HTTPMockCallout_Forex.cls
 * @Description        : 
 * @Author             : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Group              : 
 * @Last Modified By   : kumarmuthu9@gmail.com
 * @Last Modified On   : 17/6/2020, 11:00:31 am
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    18/5/2020   ChangeMeIn@UserSettingsUnder.SFDoc     Initial Version
**/
@isTest
global class HTTPMockCallout_Forex implements HttpCalloutMock {
    global HTTPResponse respond(HTTPRequest req) {        
       if(req.getEndpoint().endsWith('JPY')){
        System.assertEquals('https://api.exchangeratesapi.io/latest?base=JPY', req.getEndpoint());
        System.assertEquals('GET', req.getMethod());
        HttpResponse response = new HttpResponse();
        response.setHeader('Content-Type', 'application/json');
        response.setBody('{"rates":{"USD":0.0093464901},"base":"JPY","date":"2020-05-15"}');
        response.setStatusCode(200);
        return response;
       } else {
        System.assertEquals('https://muthu77-dev-ed.my.salesforce.com/services/data/v28.0/sobjects/CurrencyType/', req.getEndpoint());
        System.assertEquals('POST', req.getMethod());
        HttpResponse response = new HttpResponse();
        
        String ReqBody;
        ReqBody =  '{ "IsoCode" : "USD","DecimalPlaces" : 4, "ConversionRate" : 0.009, "IsActive" : "true" }';
        response.setBody(ReqBody);
        response.setHeader('Authorization', 'OAuth ' + UserInfo.getSessionId());                        
        response.setHeader('Content-Type', 'application/json');

        response.setStatusCode(200);
        return response;
       }
    }
}