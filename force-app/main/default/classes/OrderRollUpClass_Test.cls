@isTest
public class OrderRollUpClass_Test {
    
    @testSetup
    static void setupTestData(){
        //Create Accounts
        Account acct = new Account(
            name= 'Test Acct',
            Customer_ID__c = '123'
        );
        insert acct;
    }
    
    static testMethod void test_Case_trigger(){
        Account acct = [select id,name,Customer_ID__c from Account LIMIT 1];     
        //Create Order1
        List<Orders__x> OrderLst = new List<Orders__x>();
        Orders__x order1 = new Orders__x(
            ExternalId = '123',
            OrderID__c  = 1,
            CustomerID__c = '123',
            CustomerID__r = acct,
            orderDate__c = Date.today().addDays(-5),
            shippedDate__c = Date.today().addDays(-3)
        );
        OrderLst.add(order1);
        //Create Order2
        Orders__x order2 = new Orders__x(
            ExternalId = '124',
            OrderID__c  = 2,
            CustomerID__c = '123',
            CustomerID__r = acct,
            orderDate__c = Date.today().addDays(-5),
            shippedDate__c = Date.today().addDays(-3)
        );
        OrderLst.add(order2);
        Test.startTest();
        //mocking order count field value as we can't insert external order records
        acct.Order_Count__c = OrderLst.size();
        update acct;
        Test.stopTest();
        system.assertEquals(2, acct.Order_Count__c);
    }
    
    static testMethod void test_Case_future_method(){
        Account acct = [select id,name,Customer_ID__c from Account LIMIT 1];         
        List<Orders__x> OrderLst = new List<Orders__x>();
        //Create Order1
        Orders__x order1 = new Orders__x(
            ExternalId = '123',
            OrderID__c  = 1,
            CustomerID__c = '123',
            CustomerID__r = acct,
            orderDate__c = Date.today().addDays(-5),
            shippedDate__c = Date.today().addDays(-3)
        );
        OrderLst.add(order1);
        //Create Order2
        Orders__x order2 = new Orders__x(
            ExternalId = '124',
            OrderID__c  = 2,
            CustomerID__c = '123',
            CustomerID__r = acct,
            orderDate__c = Date.today().addDays(-5),
            shippedDate__c = Date.today().addDays(-3)
        );
        OrderLst.add(order2);
        List<id> accids = new List<id>();
        accids.add(acct.id);  
        Test.startTest();
        //mocking order count field value as we can't insert external order records 
        acct.Order_Count__c = OrderLst.size();
        update acct;
        //calling future method
        OrderRollUpClass.RollupOrder(accids);
        Test.stopTest();
        system.assertEquals(2, acct.Order_Count__c);
    }
}