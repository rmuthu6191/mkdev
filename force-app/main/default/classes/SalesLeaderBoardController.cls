public with sharing class SalesLeaderBoardController {
    public SalesLeaderBoardController() {

    }

@AuraEnabled
public static list<SalesLeader> getSLDashboardData(){
    list<SalesLeader> lstsalesleader = new list<SalesLeader>();
    Integer THIS_YEAR = System.Date.today().year();
    map<String,decimal> mapUserIdByamount = new map<String,decimal>();
    for(AggregateResult ag: [SELECT SUM(Amount) sum,
    ownerId
    FROM Opportunity 
    WHERE stageName ='Closed Won'
    AND CALENDAR_YEAR(CloseDate) =: THIS_YEAR
    group by ownerId
    order by sum(Amount) desc
    LIMIT 10]){
        mapUserIdByamount.put((String)ag.get('ownerID'),(Decimal)ag.get('sum'));
    }

    for(User u : [select Id,name,email,FullPhotoUrl from user where id in : mapUserIdByamount.keySet()]){
        SalesLeader sleader = new SalesLeader(u.Name,u.email,u.id,mapUserIdByamount.get(u.id),u.fullphotourl);
        lstsalesleader.add(sleader);
    }
    lstsalesleader.sort();
    return lstsalesleader;
}

@AuraEnabled
public static list<Opportunity> getlstopportunities( String ownerId){
    Integer THIS_YEAR = System.Date.today().year();

    return [select id,Name,Account.Name,CloseDate,Amount FROM opportunity 
    where ownerId = : ownerId
    and stageName ='Closed Won'
    and CALENDAR_YEAR(CloseDate) =: THIS_YEAR
    ORDER BY CloseDate DESC LIMIT 100];
    
}

}