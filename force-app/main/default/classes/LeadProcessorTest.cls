@isTest
public class LeadProcessorTest {
    
    @testsetup
    static void setup(){
            List<Lead> Leadrecords = new List<Lead>();   
        for (Integer i=0;i<200;i++) {
            Leadrecords.add(new lead(lastname='Lead '+i,company =i+' Test company'));
        }    
        insert Leadrecords;
    }
    
    @isTest
    public static void LeadTest(){           
        Test.startTest();
        LeadProcessor proc = new LeadProcessor();
        Id batchid = Database.executeBatch(proc);
        Test.stopTest();
        
        System.assertEquals(200, [select count() from Lead where LeadSource ='Dreamforce']);
        
    }
}