public class LogUtility {
    public static void messageLog (String classMethod, String customMessage, Set<id> recordids)
    {
        case logEntry = new case(
            Type = 'Message',
            Subject = customMessage,
            Description = 'Record Ids ' + recordids + '\n'
            + customMessage
        );
        insertLog(logEntry, classMethod);
    }
    public static void errorLog(String classMethod, Exception e, Set<id> recordids)
    {
        case logEntry = new case(
            Type = 'Error',
            Subject = e.getMessage(),
            Description = 'Record Ids '+ recordids + '\n'
            +'Line Number: '+e.getLineNumber()+'\n'
            +'Exception Type: '+e.getTypeName()+'\n'
            +'Stack Trace: '+ e.getStackTraceString()
        );
        insertLog(logEntry, classMethod);
    }
    public static void insertLog (case logEntry, String classMethod)
    {
 		logEntry.Origin = classMethod;
      	logEntry.Reason = UserInfo.getUserId();
        Database.DMLOptions options = new Database.DMLOptions();
        options.AllowFieldTruncation = true;
        Database.insert(logEntry, options);
        
    }
}