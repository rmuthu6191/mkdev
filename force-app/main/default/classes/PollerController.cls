public with sharing class PollerController {
    List<String> Status = new List<String> { 'Connecting to Bank', 'Authorising', 
        'Authorised', 'Complete'};
            
  	private Integer stateIdx =0;
    
    public String getPaymentState(){
        return Status[stateIdx];
    }
    
    public PageReference movePayment(){
        stateIdx++;
        return null;
    }
    
}