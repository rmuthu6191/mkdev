public with sharing class SalesLeader implements Comparable {
    public SalesLeader(String fullName,String email,String userId,  Decimal netsales,String photoURL) {
        this.fullname = fullname;
        this.email = email;
        this.userId = userId;
        this.netsales = netsales;
        this.photoURL = photoURL;
    }

@AuraEnabled 
public String fullname{get;set;}
@AuraEnabled 
public String email{get;set;}
@AuraEnabled 
public String userId{get;set;}
@AuraEnabled 
public Decimal netsales{get;set;}
@AuraEnabled 
public String photoURL {get;set;}

// implement the compareTo() method

public Integer compareTo(Object compareTo){
    SalesLeader compareTonetrevenue = (SalesLeader) compareTo;
    if(netsales == compareTonetrevenue.netsales) return 0;
    if(netsales < compareTonetrevenue.netsales) return 1;
    return -1;
}

}