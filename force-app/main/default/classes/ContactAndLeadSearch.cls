public class ContactAndLeadSearch {
    
    public static List<List< SObject>> searchContactsAndLeads(String Smith){
        List<List< SObject>> searchlist = [FIND 'Smith' IN ALL FIELDS RETURNING Contact(LastName,FirstName),Lead(LastName,FirstName)];
        return searchlist;
    }

}