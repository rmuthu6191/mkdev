public class AccountAuraClr {
@AuraEnabled
    public static List<Account> getAccountRecords() {
        return new List<Account>([Select id,Name,Phone,Type from Account ORDER BY CreatedDate desc LIMIT 10]);
    }
}