Public with sharing class csvFileReaderController {
public Blob csvFileBody{get;set;}
Public string csvAsString{get;set;}
Public String[] csvfilelines{get;set;}
Public String[] inputvalues{get;set;}
Public List<string> fieldList{get;set;}
Public List<AccountTeamMember> sObjectList{get;set;}
  public csvFileReaderController(){
    csvfilelines = new String[]{};
    fieldList = New List<string>();
    sObjectList = New List<sobject>(); 
  }
 
  Public void readcsvFile(){
       csvAsString = csvFileBody.toString();
       csvfilelines = csvAsString.split('\n');
       inputvalues = new String[]{};
       for(string st:csvfilelines[0].split(',')) 
           fieldList.add(st);   
       
       for(Integer i=1;i<csvfilelines.size();i++){
           AccountTeamMember accMemRec = new AccountTeamMember() ;
           string[] csvRecordData = csvfilelines[i].split(',');
           accMemRec.AccountID = csvRecordData[0] ;             
           accMemRec.UserID = csvRecordData[1];
           accMemRec.Teammemberrole = csvRecordData[2];                                                                              
           sObjectList.add(accMemRec);   
       }
      insert SObjectList;
  }
}