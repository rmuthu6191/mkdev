public class AccountProcessor {
    
    @future
    public static void countContacts(List<id> accID){
        
        Integer ContactCount;
        List<Account> AccList = [select id,Number_of_Contacts__c,(select id from contacts) from Account where ID IN: accID];
        
        for(Account Acc : AccList ) {
            List<contact> conlst = Acc.contacts;
            Acc.Number_of_Contacts__c = conlst.size();
        }
        
        update AccList;
        
    }

}