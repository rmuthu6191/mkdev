public with sharing class ActionParameterController {
    
    public id oppsIdWin {get; set;}
    
    public list<opportunity> getopps(){
        return [select id,name,amount,stageName,closedate from Opportunity LIMIT 50];
    }
	
    public pageReference OppstoWin(){
        Opportunity p = new Opportunity(id=oppsIdWin, stageName='Closed Won');
        update p;
      	return null;
    }
}