global class populateCountry implements Database.Batchable<sObject> {
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        
        return Database.getQueryLocator([select name,Country_of_sales__c from Account]);
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope)
    {
        list<Account> thisAcc = [select name from Account where id in :scope];
        List<Account> Acc = new List<Account>();
        
        for (Account a : thisAcc){
            a.Country_of_sales__c ='Test Country';	
            Acc.add(a);
        }
        
        update Acc;
    }
    
    global void finish(Database.BatchableContext BC)
    {
        
    }
}