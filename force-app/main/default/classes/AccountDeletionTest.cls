@isTest
public class AccountDeletionTest {
    
    @isTest static void deleteSingleAccwithOppty(){
        List<Account> a = TestDataFactory.createAccountswithOppty(1, 1);
        
        Test.startTest();
        Database.DeleteResult[] result = Database.delete(a, false);
        Test.stopTest();
        
        for(Database.DeleteResult dr : result){
            system.assert(!dr.isSuccess());
            
            system.assert(dr.getErrors().size()>0);
            
            system.assertEquals('Can\'t delete account with related opportunity', dr.getErrors()[0].getMessage());
        }
    }
        
        @isTest static void deleteSingleAccwithoutOppty(){
            List<Account> a = TestDataFactory.createAccountswithOppty(1, 0);
            
            Test.startTest();
            Database.DeleteResult[] result = Database.delete(a, false);
            Test.stopTest();
            
            for(Database.DeleteResult dr : result){
                system.assert(dr.isSuccess());
            }
        }
            
            @isTest static void deleteMultiAccwithOppty(){
                List<Account> a = TestDataFactory.createAccountswithOppty(200, 1);
                
                Test.startTest();
                Database.DeleteResult[] result = Database.delete(a, false);
                Test.stopTest();
                
                for(Database.DeleteResult dr : result){
                    system.assert(!dr.isSuccess());
                    
                    system.assert(dr.getErrors().size()>0);
                    
                    system.assertEquals('Can\'t delete account with related opportunity', dr.getErrors()[0].getMessage());
                }
            }
                
                @isTest  static void deleteMultiAccwithOutOppty(){
                    List<Account> a = TestDataFactory.createAccountswithOppty(200, 0);
                    
                    Test.startTest();
                    Database.DeleteResult[] result = Database.delete(a, false);
                    Test.stopTest();
                    
                    for(Database.DeleteResult dr : result){
                        system.assert(dr.isSuccess());
                        
                    
                    }
                    
                }
                
            }