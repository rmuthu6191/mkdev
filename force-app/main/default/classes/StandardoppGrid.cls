/*
* File Name  : StandardoppGrid
* Description: It lets user search for his/her opportunities and modify them if needed
* Author     :
* Modification Log
* =============================================================================
* Ver   Date        Author                         Modification
* -----------------------------------------------------------------------------------------------------------------------------------------
* 1.0   01/07/2014  Kushal        This Grid displays Open Opportunities of logged in users whose Opportunity record type is Standard Opportunities or Government Standard.
* 1.2   03/07/2016  Anusha        Added Status,Stage,Forecast,Amount and service Filters
* 1.3   03/20/2017  Anusha        Removed Service_Stage__c - INC1153096
* 1.4  19/02/2017  Sravan      SF-525- Removed hardcoded references of opportunity record types apart from "Registration"
*                  & "Project".
*1.5    04/23/2018  Vineela & Harish Modified the query to display the records for which user has Edit access to the Opportunity
                    Harish
*/
 
      
public with sharing class StandardoppGrid {
    
    public integer Year1;
    public string userMode;
    public string oppOwner;
    public String Message {
        get;
        set;
    }
    public boolean firstShipFlag {
        get;
        set;
    }
    public boolean riskFlag {
        get;
        set;
    }
    public Opportunity opportunity {
        get;
        set;
    }
    public boolean popup {
        get;
        set;
    }
    public User user;
    public String theater {
        get;
        set;
    }
    public Id tempOppid {
        get;
        set;
    }
    public String tempStatus {
        get;
        set;
    }
    public String reasonForWinLoss {
        get;
        set;
    }
    public Boolean govtFlag {
        get;
        set;
    }
    public Map < Id, String > oppReasonMap {
        get;
        set;
    }
    public String oppName {
        get;
        set;
    }
    public String errMsg {
        get;
        set;
    }
    public Boolean successMsg {
        get;
        set;
    }
    //Opportunity search changes to add more filters
    public String searchStage {
        get;
        set;
    }
    public String searchforcast {
        get;
        set;
    }
    public String searchStatus {
        get;
        set;
    }
    public Decimal searchAmount {
        get {
            return (searchAmount == 0) ? null : searchAmount;
        }
        set;
    }
    
    string Qry;
    String manager_ID = UserInfo.getUserId(); // added as part of Forecast project -Anusha
    public String searchService {
        get;
        set;
    }
    public String search_Name {
        get;
        set;
    }
    public String search_Owner {
        get;
        set;
    } //added by Anusha 
    Set < ID > OpShareIds = new Set < ID > ();
    public boolean checkresult = true;
    //end - Added by Anusha
    
    //added for pagination
    
    public Integer noOfRecords {
        get;
        set;
    }
    public Integer size {
        get;
        set;
    }
    
    public ApexPages.StandardSetController setCon {
        get;
        set;
    }
    public List < SelectOption > paginationSizeOptions {
        get;
        set;
    }
    //end
    Public void setYear1(integer Year1) {
        this.Year1 = Year1;
    }
    
    public Integer getYear1() {
        return Year1;
    }
    
    Public void setuserMode(string userMode) {
        this.userMode = userMode;
    }
    
    Public void setoppOwner(string oppOwner) {
        this.oppOwner = oppOwner;
    }
    
    Public string getoppOwner() {
        return oppOwner;
    }
    
    public string getuserMode() {
        return userMode;
    }
    
    //Declaration
    //  transient public List < Opportunity > opportunityList;
    List < SelectOption > Owner;
    Id guyId = UserInfo.getUserID(); // current User
    
    Set < String > originalvalues = new Set < String > ();
    Public List < string > leftselected {
        get;
        set;
    }
    Public List < string > rightselected {
        get;
        set;
    }
    Set < string > leftvalues = new Set < string > ();
    Set < string > rightvalues = new Set < string > ();
    
    Public String search {
        get;
        set;
    } //search text
    
    //constructor capturing the serach text from parameters for future query use
    Public StandardoppGrid() {
        successMsg = false;
        popup = false;
        search = ApexPages.currentPage().getParameters().get('searchText');
        oppReasonMap = new Map < Id, String > ();
        leftselected = new List < String > ();
        rightselected = new List < String > ();
        Integer temp_size = Integer.valueOf(system.label.StandardOppGrid_PageSize);
        
        size = temp_size;
        
        paginationSizeOptions = new List < SelectOption > ();
        paginationSizeOptions.add(new SelectOption('10', '10'));
        paginationSizeOptions.add(new SelectOption('20', '20'));
        paginationSizeOptions.add(new SelectOption('30', '30'));
        reset();
    }
    
    public void displayPopup() {
        system.debug('tempStatus  --> ' + tempStatus);
        system.debug('popup  --> ' + popup);
        system.debug(' tempOppid --> ' + tempOppid);
        
        if (tempStatus == 'Won' || tempStatus == 'Lost' || tempStatus == 'No Pursuit') {
            popup = true;
            opportunity = [SELECT id, name, Status__c, Reason_for_Win_or_Loss__c, Owner.Name, ForecastCategoryName FROM Opportunity WHERE id =: tempOppid LIMIT 1];
            system.debug('oppId ' + tempOppid + 'opp status ' + opportunity.status__c + ' opp name ' + oppName);
            oppName = opportunity.name;
            //   For populating the Reason for Win/Loss values
            originalvalues.clear();
            rightvalues.clear();
            leftvalues.clear();
            if (tempStatus == 'Won') {
                //system.debug(' opportunity.ForecastCategoryName' +opportunity.ForecastCategoryName);
                
                Map < String, WinReasons__c > mapForCustomSetting = WinReasons__c.getAll();
                for (WinReasons__c reasonMap: mapForCustomSetting.values()) {
                    originalvalues.add(reasonMap.Reasons__c);
                }
            } else if (tempStatus == 'Lost') {
                Map < String, LostReasons__c > mapForCustomSetting = LostReasons__c.getAll();
                for (LostReasons__c reasonMap: mapForCustomSetting.values()) {
                    originalvalues.add(reasonMap.Reasons__c);
                }
            } else if (tempStatus == 'No Pursuit') {
                Map < String, NoPursuitReasons__c > mapForCustomSetting = NoPursuitReasons__c.getAll();
                for (NoPursuitReasons__c reasonMap: mapForCustomSetting.values()) {
                    originalvalues.add(reasonMap.Reasons__c);
                }
            }
            leftselected = new List < String > ();
            rightselected = new List < String > ();
            leftvalues.addAll(originalvalues);
        } else
            popup = false;
    }
    
    
    public PageReference closePopup() {
        try {
            if (rightvalues.isEmpty()) {
                ApexPages.Message errMsg = new ApexPages.Message(ApexPages.severity.ERROR, 'The Opportunity cannot be closed without a value for the Reason for Win/Loss');
                ApexPages.addmessage(errMsg);
                popup = true;
            } else {
                popup = false;
                opportunity.StageName = 'Secure';
                opportunity.Status__c = tempStatus;
                system.debug('Right Values' + rightvalues);
                reasonForWinLoss = null;
                for (String s: rightvalues) {
                    if (reasonForWinLoss != null)
                        reasonForWinLoss = reasonForWinLoss + ';' + s;
                    else
                        reasonForWinLoss = s;
                }
                system.debug('reasons ' + reasonForWinLoss);
                oppReasonMap.put(opportunity.id, reasonForWinLoss);
                //  update opportunity;
            }
        } catch (Exception e) {
            ApexPages.addmessages(e);
            system.debug('wrong');
        }
        return null;
    }
    
    
    //reset the opportunity Loading
    public PageReference reset() {
        //string Qry ;
        //checkresult = true;
        Year1 = System.Today().year(); //added as a art of INC1821063 by Vineela
        user = [SELECT ID, Primary_Business_Group__c, Theater_Code__c FROM User WHERE id =: UserInfo.getUserID()];
        theater = user.Theater_Code__c;
        String LoggedInUserID = user.ID;
        if (theater == 'EMEA' && user.Primary_Business_Group__c == 'Government')
            riskFlag = true;
        else
            riskFlag = false;
        
        system.debug('1Vinss  -->' + Year1);
        system.debug('2  --> ' + theater);
        system.debug('3  --> ' + LoggedInUserID);
        
        // Added by Kushal - Fulfillment field as a part of Opportunity Management
        
        Qry = 'SELECT id,UserRecordAccess.HasEditAccess,accountid,Owner.Name,Account.Name ,Fulfillment__c, owner.Theater_Code__c , name, ForecastCategoryName, primary_technology__c, Amount, closedate,Pull_Forward_Date__c,Award_Date__c,In_House__c,Orders_Risk_Upside__c,Prebuild_Status__c,Forecast_Notes__c,stagename ,Owner_Theater__c,Project_Name__c,First_Ship_Date__c, Revenue_Date__C,SBL_Fulfillment__c,Disti_Book_Date__c,Num_Product_Families__c,RecordTypeID,Reason_for_Win_or_Loss__c,Status__c,Opportunity_Forecast__c,MSI_Business_Group__c,Channel_Opportunity__c,mh_Place_in_Sales_Funnel__c  FROM opportunity'; // Added "Go to market" field -Harish   //Added "Pull_Forward_Date__c,Forecast_Notes__c" field -Sampath
        Qry = Qry + ' WHERE ((RecordType.developerName=\'Government_Standard\')';
        Qry = Qry + ' AND ((Status__c = \'Active\' or stagename =\'No Hardware\')'; 
        Qry = Qry + ' AND (Calendar_Year(Closedate) =:year1)';
        Qry = Qry + ' AND (owner.Theater_Code__c=:theater)';
        
        if (user.Primary_Business_Group__c == 'Enterprise') {
            Qry = Qry + ' AND (MSI_Business_Group__c =\'Enterprise\'))';
            firstShipFlag = true;
            govtFlag = false;
        } else {
            Qry = Qry + ' AND (MSI_Business_Group__c = \'Government\'))';
            firstShipFlag = false;
            govtFlag = true;
        }
        //Qry = Qry + ') LIMIT 5000'; 
        Qry = Qry + ')';
        System.debug('Debugs%%%%' + Qry);
        //Added by Harish as part of INC1790205
        List<sObject> sobjList = new List<sObject>();
        sobjList = Database.query(Qry+' LIMIT 5000');
        //sobjList = Database.query(Qry);
        Set<Id> oppIds = new Set<Id>();
        for(Opportunity opp:(List<Opportunity>)sobjList){
            if(opp.UserRecordAccess.HasEditAccess == TRUE){
                oppIds.add(opp.Id);
            }   
        }
        
        Qry = Qry+' AND Id IN:oppIds ORDER BY closedate,name,account.name,amount,Forecast_Notes__c LIMIT 5000';
        system.debug('Final Query### = >' + Qry);
        //ended by Harish as part of INC1790205
        setCon = new ApexPages.StandardSetController(Database.getQueryLocator(Qry));
        
        return null;
    }
    
    
    
    //Populate the opportunity when the page is loaded. Current year will be set as the default year
    public List < Opportunity > getAccounts() {
        
        try {
            if (setCon.getResultSize() == 0) {
                
                if (checkresult == true) {
                    Year1 = System.Today().year();
                    reset();
                }
                
            }
            
            // return opportunityList;
            if (setCon.getResultSize() > 0) {
                setCon.setPageSize(size);
                
                noOfRecords = setCon.getResultSize();
                return (List < Opportunity > ) setCon.getRecords();
                // System.debug('get records @@@@@@@@@' + (List < Opportunity > ) setCon.getRecords());
            } else {
                return null;
            }
            //return null;
        } catch (Exception e) {
            return null;
        }
    }
    
    //changes added by Anusha to search all the filters
    
    public PageReference runfilterSearch() {
        Set<Id> oppIds = new Set<Id>(); //Added by Vineela as part of INC1790205 
        System.debug('DEBUG ::: Entered Method Filter Search');    
        Set<Id> oppIdSet = new Set<Id>();
        //string Qry ;
        
        user = [SELECT ID, Primary_Business_Group__c, Theater_Code__c FROM User WHERE id =: UserInfo.getUserID()];
        theater = user.Theater_Code__c;
        String LoggedInUserID = user.ID;
        
        Set<Id> oppIdfromLineitems = new Set<Id>();             
        if (searchService == 'Service Only') {
            
            for(Opportunity opp :  [Select Id,Name,Owner.Name,(SELECT id,PricebookEntry.Product2Id, PricebookEntry.Product2.Product_type__c FROM OpportunityLineItems where Product2.Product_type__c ='Services')from Opportunity Where ID in (Select OpportunityId from OpportunityLineItem where Product2.Product_type__c ='Services' ) limit 1000])
            {  
                    oppIdSet.add(opp.Id);    

            }
        }
        
        
        if (theater == 'EMEA' && user.Primary_Business_Group__c == 'Government')
            riskFlag = true;
        else
            riskFlag = false;
        system.debug('1  -->' + Year1);
        system.debug('2  --> ' + theater);
        system.debug('3  --> ' + LoggedInUserID);
        Qry = 'select id,UserRecordAccess.HasEditAccess,accountid,mh_Place_in_Sales_Funnel__c , Account.Name,Owner.Name ,Fulfillment__c, owner.Theater_Code__c , name, ForecastCategoryName, primary_technology__c, Amount, closedate,Pull_Forward_Date__c,Award_Date__c,In_House__c,Orders_Risk_Upside__c,Prebuild_Status__c,Forecast_Notes__c,stagename ,Owner_Theater__c,Project_Name__c,First_Ship_Date__c, Revenue_Date__C,SBL_Fulfillment__c,Disti_Book_Date__c,Num_Product_Families__c,RecordTypeID,Reason_for_Win_or_Loss__c,Status__c,Opportunity_Forecast__c,MSI_Business_Group__c,Channel_Opportunity__c  from opportunity'; // Added "Go to market" field -Harish   //Added "Pull_Forward_Date__c,Forecast_Notes__c" field -Sampath
        Qry = Qry + ' where (((RecordType.developerName=\'Government_Standard\')';
        Qry = Qry + ' and ((Calendar_Year(Closedate) =:year1))';
        Qry = Qry + ' and (owner.Theater_Code__c=:theater)';
        // Qry=Qry + ' and (owner.id = :LoggedInUserID  or Owner.ManagerId=:manager_ID)';
        Qry = Qry + ' and (Amount >=: searchAmount)';
        Qry = Qry + ' and (Status__c =: searchStatus)';
        
        
        System.Debug('275 Opportunity Owner ' + OppOwner);
        System.debug('qry::' + Qry);
        if (user.Primary_Business_Group__c == 'Enterprise') {
            Qry = Qry + ' and (MSI_Business_Group__c =\'Enterprise\'))';
            firstShipFlag = true;
            govtFlag = false;
        } 
        else {
            Qry = Qry + ' and (MSI_Business_Group__c = \'Government\'))';
            firstShipFlag = false;
            govtFlag = true;
        }
        
        
        System.debug('Debug Values :: ' + searchService + searchStage + 'Debug Values ::' + searchforcast + searchStatus + searchAmount);
        
        if (searchService == 'All' && searchStage == 'All' && searchforcast == 'All' && searchStatus == 'Active' && searchAmount == Null && (search_Name == Null || search_Name == '') && (search_Owner == Null || search_Owner == '')) {
            checkresult = false;
            //Qry = Qry +') LIMIT 5000'; //Added by Vineela as part of INC1821063
            Qry = Qry + ') ORDER BY closedate,name,account.name,amount,Forecast_Notes__c LIMIT 5000' ;// Added by Vineela for present issue
            //reset(); //Commented as a part of INC1821063 by Vineela
            
        } 
        else if (searchService == 'All' && searchStage == 'All' && searchforcast == 'All' && searchStatus == 'Active' && searchAmount == Null && (search_Name != Null || search_Name != '') && (search_Owner != Null || search_Owner != '')) {
            //Added by Vineela as a part of SF-830(INC1821301)
            String Searchname = String.escapeSingleQuotes(Search_Name);
            System.debug('Searchname********' +Searchname);
            checkresult = false;
            Qry = Qry + 'and((name like \'%' + Searchname + '%\')' + 'OR (Account.Name like \'%' + Searchname + '%\'))';
            Qry = Qry + 'and(Owner.Name like \'%' + search_Owner + '%\'))';
            //Added by Vineela as part of INC1790205
            List<sObject> sobjList = new List<sObject>();
        sobjList = Database.query(Qry);
        for(Opportunity opp:(List<Opportunity>)sobjList) {
            if(opp.UserRecordAccess.HasEditAccess == TRUE) {
                oppIds.add(opp.Id);
                
            }   
        }
            Qry = Qry + 'and (ID IN: oppIds)';
            Qry = Qry + ' order by closedate,name,account.name,amount';
        } 
        //ended by Vineela as part of INC1790205
        
        else if (searchService == 'All' && searchStage == 'All' && searchforcast == 'All' && searchStatus == 'Active' 
                 && searchAmount != Null) {
            checkresult = false;
            if (search_Name != null && search_Name != '') {
                String Searchname = String.escapeSingleQuotes(Search_Name);
                Qry = Qry + 'and((name like \'%' + Searchname + '%\')' + 'OR (Account.Name like \'%' + Searchname + '%\'))';
            }
            if (search_Owner != null && search_Owner != '') {
                Qry = Qry + 'and(Owner.Name like \'%' + search_Owner + '%\')';
            }
            Qry = Qry + ') order by closedate  ';
            Qry= Qry +' limit 5000 ';
            
            system.debug(qry);
            system.debug('Final Query ' + qry);
            System.debug('Condition All Amt not eqauls to null');
        } 
        
        else if (searchService == 'All' && searchStage == 'All' && searchforcast == 'All' && searchStatus != 'Active') {
            checkresult = false;
            if (search_Name != null && search_Name != '') {
                String Searchname = String.escapeSingleQuotes(Search_Name);
                Qry = Qry + 'and((name like \'%' + Searchname + '%\')' + 'OR (Account.Name like \'%' + Searchname + '%\'))';
            }
            if (search_Owner != null && search_Owner != '') {
                Qry = Qry + 'and(Owner.Name like \'%' + search_Owner + '%\')';
            }
            Qry = Qry + 'and (Status__c =: searchStatus)';
            Qry = Qry + ') order by closedate,name,account.name,amount';
            Qry= Qry +' limit 5000 ';
            // Qry=Qry+'LIMIT :LimitSize OFFSET :OffsetSize';
            system.debug(qry);
            system.debug('Final Query ' + qry);
            //opportunityList = Database.Query(qry);
            System.debug('Condition 1');
        } 
        	
        else if ((searchService == 'All') && (searchStage == 'All') && (searchforcast != 'All')) {
            checkresult = false;
            if (search_Name != null && search_Name != '') {
                String Searchname = String.escapeSingleQuotes(Search_Name);
                Qry = Qry + 'and((name like \'%' + Searchname + '%\')' + 'OR (Account.Name like \'%' + Searchname + '%\'))';
            }
            if (search_Owner != null && search_Owner != '') {
                Qry = Qry + 'and(Owner.Name like \'%' + search_Owner + '%\')';
            }
            
            Qry = Qry + ' and (ForecastCategoryName =: searchforcast)';
            
            Qry = Qry + ') order by closedate,name,account.name,amount ';
            Qry= Qry +' limit 5000 ';
            // Qry=Qry+'LIMIT :LimitSize OFFSET :OffsetSize';
            system.debug(qry);
            system.debug('Final Query ' + qry);
            // opportunityList = Database.Query(qry);
            System.debug('Condition 2');
        } 
        
        else if ((searchService == 'All') && (searchStage != 'All') && (searchforcast == 'All')) {
            checkresult = false;
            if (search_Name != null && search_Name != '') {
                String Searchname = String.escapeSingleQuotes(Search_Name);
                Qry = Qry + 'and((name like \'%' + Searchname + '%\')' + 'OR (Account.Name like \'%' + Searchname + '%\'))';
            }
            if (search_Owner != null && search_Owner != '') {
                Qry = Qry + 'and(Owner.Name like \'%' + search_Owner + '%\')';
            }
            Qry = Qry + ' and (stagename =: searchStage)';
            
            Qry = Qry + ') order by closedate,name,account.name,amount ';
            Qry= Qry +' limit 5000 ';
            //Qry=Qry+'LIMIT :LimitSize OFFSET :OffsetSize';
            system.debug(qry);
            system.debug('Final Query ' + qry);
            //opportunityList = Database.Query(qry);
            System.debug('COndition 3');
        } 
        
        else if ((searchService == 'All') && (searchStage != 'All') && (searchforcast != 'All')) {
            checkresult = false;
            if (search_Name != null && search_Name != '') {
                String Searchname = String.escapeSingleQuotes(Search_Name);
                Qry = Qry + 'and((name like \'%' + Searchname + '%\')' + 'OR (Account.Name like \'%' + Searchname + '%\'))';
            }
            if (search_Owner != null && search_Owner != '') {
                Qry = Qry + 'and(Owner.Name like \'%' + search_Owner + '%\')';
            }
            Qry = Qry + ' and (stagename =: searchStage)';
            Qry = Qry + ' and (ForecastCategoryName =: searchforcast)';
            Qry = Qry + ' and (Status__c =: searchStatus)';
            Qry = Qry + ') order by closedate,name,account.name,amount ';
            
            system.debug(qry);
            system.debug('Final Query ' + qry);
            // opportunityList = Database.Query(qry);
            
            System.debug('COndition not equals all');
        } 
        
        else if ((searchService == 'Service Only') && (searchStage == 'All') && (searchforcast == 'All')) {
            checkresult = false;
            if (search_Name != null && search_Name != '') {
                String Searchname = String.escapeSingleQuotes(Search_Name);
                Qry = Qry + 'and((name like \'%' + searchName + '%\')' + 'OR (Account.Name like \'%' + searchName + '%\'))';
            }
            if (search_Owner != null && search_Owner != '') {
                Qry = Qry + 'and(Owner.Name like \'%' + search_Owner + '%\')';
            }
            Qry = Qry + 'and (ID IN: OppIdSet)';
            Qry = Qry + ') order by closedate,name,account.name,amount';
            Qry= Qry +' limit 5000 ';
            // Qry=Qry+'LIMIT :LimitSize OFFSET :OffsetSize';
            system.debug(qry);
            system.debug('Final Query ' + qry);
            //opportunityList = Database.Query(qry);
            System.debug('COndition 4');
            
            
        } 
        
        
        else if ((searchService == 'Service Only') && (searchStage == 'All') && (searchforcast != 'All')) {
            checkresult = false;
            if (search_Name != null && search_Name != '') {
                String Searchname = String.escapeSingleQuotes(Search_Name);
                Qry = Qry + 'and((name like \'%' + searchName + '%\')' + 'OR (Account.Name like \'%' + searchName + '%\'))';
            }
            if (search_Owner != null && search_Owner != '') {
                Qry = Qry + 'and(Owner.Name like \'%' + search_Owner + '%\')';
            }
            
            Qry = Qry + 'and (ID IN: OppIdSet)';
            Qry = Qry + ' and (ForecastCategoryName =: searchforcast)';
            System.Debug('Opportunity Owner ' + OppOwner);
            Qry = Qry + ') order by closedate,name,account.name,amount ';
            Qry= Qry +' limit 5000 ';
            // Qry=Qry+'LIMIT :LimitSize OFFSET :OffsetSize';
            system.debug(qry);
            system.debug('Final Query ' + qry);
            //opportunityList = Database.Query(qry);
            System.debug('COndition 5');
            
        } 
        
        
        else if ((searchService == 'Service Only') && (searchStage != 'All') && (searchforcast == 'All')) {
            checkresult = false;
            if (search_Name != null && search_Name != '') {
                String Searchname = String.escapeSingleQuotes(Search_Name);
                Qry = Qry + 'and((name like \'%' + searchName + '%\')' + 'OR (Account.Name like \'%' + searchName + '%\'))';
            }
            if (search_Owner != null && search_Owner != '') {
                Qry = Qry + 'and(Owner.Name like \'%' + search_Owner + '%\')';
            }
            Qry = Qry + ' and (stagename =: searchStage)';
            Qry = Qry + 'and (ID IN: OppIdSet)';
            System.Debug('Opportunity Owner ' + OppOwner);
            Qry = Qry + ') order by closedate,name,account.name,amount ';
            Qry= Qry +' limit 5000 ';
            //Qry=Qry+'LIMIT :LimitSize OFFSET :OffsetSize';
            system.debug(qry);
            system.debug('Final Query Vins ' + qry);
            //opportunityList = Database.Query(qry);
            System.debug('COndition 6');
        } 
        
        
        else if ((searchService == 'Service Only') && (searchStage != 'All') && (searchforcast != 'All')) {
            checkresult = false;
            if (search_Name != null && search_Name != '') {
                String Searchname = String.escapeSingleQuotes(Search_Name);
                Qry = Qry + 'and((name like \'%' + searchName + '%\')' + 'OR (Account.Name like \'%' + searchName + '%\'))';
            }
            if (search_Owner != null && search_Owner != '') {
                Qry = Qry + 'and(Owner.Name like \'%' + search_Owner + '%\')';
            }
            Qry = Qry + ' and (stagename =: searchStage)';
            Qry = Qry + ' and (ForecastCategoryName =: searchforcast)';
            Qry = Qry + 'and (ID IN: OppIdSet)';
            System.Debug('Opportunity Owner ' + OppOwner);
            Qry = Qry + ') order by closedate,name,account.name,amount ';
            Qry= Qry +' limit 5000 ';
            // Qry=Qry+'LIMIT :LimitSize OFFSET :OffsetSize';
            system.debug(qry);
            system.debug('Final Query ' + qry);
            //opportunityList = Database.Query(qry);
            System.debug('COndition 7');
        }
        System.debug('Qry********' +Qry);
        setCon = new ApexPages.StandardSetController(Database.getQueryLocator(Qry));
        
        return null;
    }
    
    public void clear() {
        
        searchStage = 'All';
        searchforcast = 'All';
        searchService = 'All';
        searchAmount = null;
        searchStatus = 'Active';
        search_Name = null;
        Year1 = System.Today().year();
        
        reset();
    }
    
    
    //end changes Anusha
    
    
    //commit the changes to the Database
    public PageReference save() {
        try {
            List < Opportunity > oppList = (List < Opportunity > ) setCon.GetRecords();
            List < Opportunity > TempoppList = new List < Opportunity > ();
            for (Opportunity o: oppList) {
                if (o.Status__c != 'Active') {
                    o.Reason_for_Win_or_Loss__c = oppReasonMap.get(o.id);
                }
                //Added by Swami Start
                if (o.StageName == 'Execute & Expand / Won') {
                    o.ForecastCategoryName = 'Closed';
                }
                TempoppList.add(o); // Added by Harish
                
            }
            update TempoppList; // Added by Harish
            successMsg = true;
            reset();
            
            ApexPages.addmessage(new ApexPages.Message(ApexPages.severity.INFO, 'Opportunities Updated Successfully'));
        } catch (DmlException e) {
            successMsg = false;
            ApexPages.addmessages(e);
        }
        //  system.debug(' opportunity.ForecastCategoryName' +opportunity.ForecastCategoryName);
        return null;
        
    }
    
    public PageReference add() {
        //opportunityList.add(New Opportunity());
        return null;
    }
    
    public PageReference cancel() {
        return new PageReference('/' + ApexPages.currentPage().getParameters().get('id'));
    }
    
    //Added for making the Reason for Win/Loss field dynamic
    
    
    
    //Populate all the reason for Win/Loss values
    public List < SelectOption > getunSelectedValues() {
        List < SelectOption > options = new List < SelectOption > ();
        List < string > tempList = new List < String > ();
        tempList.addAll(leftvalues);
        tempList.sort();
        for (string s: tempList)
            options.add(new SelectOption(s, s));
        return options;
    }
    
    public List < SelectOption > getSelectedValues() {
        List < SelectOption > options1 = new List < SelectOption > ();
        List < string > tempList = new List < String > ();
        tempList.addAll(rightvalues);
        tempList.sort();
        for (String s: tempList)
            options1.add(new SelectOption(s, s));
        return options1;
    }
    
    public PageReference selectclick() {
        rightselected.clear();
        for (String s: leftselected) {
            leftvalues.remove(s);
            rightvalues.add(s);
        }
        return null;
        
    }
    
    public PageReference unselectclick() {
        leftselected.clear();
        for (String s: rightselected) {
            rightvalues.remove(s);
            leftvalues.add(s);
        }
        return null;
    }
    
    //Ends here
    
    //Populate all the year for which the opportunities are available.
    public List < SelectOption > getItems() {
        System.debug('Entered Items');
        List < SelectOption > options = new List < SelectOption > ();
        String strYear;
        List < aggregateResult > RTA = new List < aggregateResult > ();
        System.debug('Entered Items Anusha*****' + user);
        If(user.Primary_Business_Group__c == 'Government') {
            RTA = [select Calendar_Year(Closedate) Year2 from opportunity where(RecordType.developerName = 'Government_Standard') and(Status__c = 'Active') and(owner.Theater_Code__c =: theater) group by Calendar_Year(Closedate) order by Calendar_Year(Closedate) Desc];
            System.debug('^^^^^^^^^^^^^^' + RTA);
        }
        Else {
            RTA = [select Calendar_Year(Closedate) Year2 from opportunity where (Status__c = 'Active') and(owner.Theater_Code__c =: theater) group by Calendar_Year(Closedate) order by Calendar_Year(Closedate) Desc];
        }
        for (aggregateResult results: RTA) {
            strYear = String.Valueof(results.get('year2'));
            options.add(new SelectOption(strYear, strYear));
        }
        return options;
    }
    
    //Populate all the User mode picklist to toggle between Team and personal opportunities.
    public list < SelectOption > getlstUM() {
        List < SelectOption > UM = new list < Selectoption > ();
        UM.add(new SelectOption('0', 'My Team Opportunities'));
        UM.add(new SelectOption('1', 'My Opportunities'));
        If(userMode == null) {
            userMode = '0';
        }
        return UM;
    }
    
    public list < SelectOption > getlstOwner() {
        FetchOwner();
        return Owner;
    }
    
    //Populate all the Team members Name for selecting the opportunities by subordinate
    Public void FetchOwner() {
        string strOwner;
        string strName;
        Owner = new list < Selectoption > ();
        Owner.add(new SelectOption('0', 'All'));
        If(userMode == '0') {
            List < aggregateResult > RTA = [select ownerid owner, owner.Name LstName from opportunity where(
                RecordType.developerName = 'Government_Standard') and(Status__c = 'Active') and(owner.Theater_Code__c =: theater) group by ownerid, owner.Name order by owner.Name asc limit 10000];
            for (aggregateResult results: RTA) {
                strOwner = String.Valueof(results.get('owner'));
                strName = String.Valueof(results.get('LstName'));
                Owner.add(new SelectOption(strOwner, strName));
            }
        }
        Else {
            owner.clear();
            Owner.add(new SelectOption('0', 'All'));
        }
    }
    // Added by Anusha to add column Filters for Opportunity Grid
    
    
    public List < SelectOption > Stages {
        get {
            List < SelectOption > options = new List < SelectOption > ();
            Schema.Describefieldresult result = Schema.sObjectType.Opportunity.fields.StageName;
            
            result = result.getSObjectField().getDescribe();
            
            List < Schema.PicklistEntry > ple = result.getPicklistValues();
            for (Schema.PicklistEntry f: ple) {
                if ((f.getValue() != 'No Hardware') && (f.getValue() != 'Align') && (f.getValue() != 'Position') && (f.getValue() != 'Design') && (f.getValue() != 'Validate') && (f.getValue() != 'Execute')) {
                    options.add(new SelectOption(f.getLabel(), f.getValue()));
                }
            }
            return options;
        }
    }
    
    public List < SelectOption > forcastCategory {
        get {
            List < SelectOption > options = new List < SelectOption > ();
            Schema.Describefieldresult result = Schema.sObjectType.Opportunity.fields.ForecastCategoryName;
            
            result = result.getSObjectField().getDescribe();
            
            List < Schema.PicklistEntry > ple = result.getPicklistValues();
            
            for (Schema.PicklistEntry f: ple) {
                if ((f.getValue() != 'Do Not Use')) {
                    options.add(new SelectOption(f.getLabel(), f.getValue()));
                }
            }
            return options;
        }
    }
    
    public List < SelectOption > Opportunitystatus {
        get {
            List < SelectOption > options = new List < SelectOption > ();
            Schema.Describefieldresult result = Schema.sObjectType.Opportunity.fields.Status__c;
            
            result = result.getSObjectField().getDescribe();
            
            List < Schema.PicklistEntry > ple = result.getPicklistValues();
            
            for (Schema.PicklistEntry f: ple)
                options.add(new SelectOption(f.getLabel(), f.getValue()));
            return options;
        }
    }
    
    public List < SelectOption > FilterService {
        get {
            List < SelectOption > options = new List < SelectOption > ();
            Schema.Describefieldresult result = Schema.sObjectType.Opportunity.fields.FilterService__c;
            
            result = result.getSObjectField().getDescribe();
            
            List < Schema.PicklistEntry > ple = result.getPicklistValues();
            
            for (Schema.PicklistEntry f: ple)
                options.add(new SelectOption(f.getLabel(), f.getValue()));
            return options;
        }
    }
    // end Opp Grid Changes
    
    /**
* refreshPageSize
* Changes the size of Pagination.

*/
    public void refreshPageSize() {
        setCon.setPageSize(size);
        
    }
    
    public void NextMethod() {
        setCon.next();
    }
    
    public void PreviousMethod() {
        setCon.previous();
    }
    
    public void FirstMethod() {
        setCon.first();
    }
    
    public void LastMethod() {
        setCon.last();
    }
    
    
}