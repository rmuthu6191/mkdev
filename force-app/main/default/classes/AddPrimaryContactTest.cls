@isTest
public class AddPrimaryContactTest {

    @testSetup
    static void setup(){
        List<Account> acc = new List<Account>() ;
        
        for(integer i =0; i<100; i++){
            if(i<50){
                acc.add(new Account(Name='AC'+i,BillingState='NY'));
            }else{
                acc.add(new Account(Name='AC'+i,BillingState='CA'));
            }            
        }
        insert acc;
    }
    
    @istest
    public static void testAddPrimaryContactTest(){
        Contact Con = new Contact(LastName ='Test Contact');
        AddPrimaryContact apc = new AddPrimaryContact(Con,'CA');        
        Test.startTest();
        system.enqueueJob(apc);
        Test.stopTest();
        
    }
}