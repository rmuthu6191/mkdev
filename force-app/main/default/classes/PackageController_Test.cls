@isTest
private class PackageController_Test{
    @testSetup
    static void setupTestData(){
        test.startTest();
        List<Package__c> package_Obj = new List<Package__c>();
        for(integer i=0;i<5;i++){
            Package__c pkg = new Package__c();
            pkg.Name = 'Package '+i;
            pkg.Namespace__c = 'Package Namespace '+i;
            pkg.Latest_Version__c='Latest Version '+i;
            package_Obj.add(pkg);
        }
        Insert package_Obj;
        test.stopTest();
    }
    /*Test method for Inserting new record*/
    static testMethod void test_InsertNew_UseCase1(){
        user currUser = [select Id,Name from User where Id=:UserInfo.getUserID()];
        system.runAs(currUser){
            PackageController obj01 = new PackageController();
            obj01.pkgnew = new Package__C(Name='Test Pkg',Latest_Version__c ='120.25',Namespace__c='Test Namespace');
            obj01.InsertNew();
        }
    }
    /*Test method for Inserting new record with inactive owner*/
    static testMethod void test_InsertNew_UseCase2(){
        user currUser = [select Id,Name from User where Id=:UserInfo.getUserID()];
        system.runAs(currUser){
            PackageController obj01 = new PackageController();
            obj01.pkgnew = new Package__C(Name='Test Pkg',Latest_Version__c ='120.25',Namespace__c='Test Namespace',ownerid='00528000005TZLo');// id of user that is inactive
            obj01.InsertNew();
        }
    }
    
     /*Test method to check schema isCreateable */
    static testMethod void test_InsertNew_UseCase3(){
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User' limit 1] ; //This profile don't have CRED permission for Package__C
        User usr = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                            EmailEncodingKey='UTF-8', FirstName='VTest',LastName='Testing', LanguageLocaleKey='en_US', 
                            LocaleSidKey='en_US', ProfileId = p.Id, TimeZoneSidKey='America/Los_Angeles', UserName='sssstandarduser@testorg.com');
        insert usr ;
        
        system.runAs(usr){
            PackageController obj01 = new PackageController();
            obj01.pkgnew = new Package__C(Name='Test Pkg',Latest_Version__c ='120.25',Namespace__c='Test Namespace');
            obj01.InsertNew();
        }
    }
    /*Test method to update records*/
    static testMethod void test_save_UseCase1(){
        user currUser = [select Id,Name from User where Id=:UserInfo.getUserID()];
        system.runAs(currUser){
        PackageController obj01 = new PackageController();
         obj01.listWrapper = new List<PackageController.WrapperClass>();
         for(Package__c pkg : [Select id, name, Latest_Version__c,Namespace__c from Package__c]) {
           obj01.listWrapper.add(new PackageController.WrapperClass(pkg));
        }
       obj01.allBool = true;
        obj01.selectAll();
        obj01.editAction();
        obj01.save();
    }
   }
    /*Test method to update record which has inactive owner*/
    static testMethod void test_save_UseCase2(){
        user currUser = [select Id,Name from User where Id=:UserInfo.getUserID()];
        system.runAs(currUser){
        PackageController obj01 = new PackageController();
         obj01.listWrapper = new List<PackageController.WrapperClass>();
         for(Package__c pkg : [Select id, name, Latest_Version__c,Namespace__c from Package__c]) {
           obj01.listWrapper.add(new PackageController.WrapperClass(pkg));
        }
        Package__c pkg1 = new Package__C(Name='Test Pkg',Latest_Version__c ='120.25',Namespace__c='Test Namespace',ownerid='00528000005TZLo');// id of user that is inactive
            obj01.listWrapper.add(new PackageController.WrapperClass(pkg1));
        obj01.allBool = true;
        obj01.selectAll();
        obj01.editAction();
        obj01.save();
    }
   }
    /*Test method to check schema isUpdateable */
       static testMethod void test_save_UseCase3(){
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User' limit 1] ; //This profile don't have CRED permission for Package__C
        User usr = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                            EmailEncodingKey='UTF-8', FirstName='VTest',LastName='Testing', LanguageLocaleKey='en_US', 
                            LocaleSidKey='en_US', ProfileId = p.Id, TimeZoneSidKey='America/Los_Angeles', UserName='sssstandarduser@testorg.com');
        insert usr ;
        system.runAs(usr){
        PackageController obj01 = new PackageController();
         obj01.listWrapper = new List<PackageController.WrapperClass>();
         for(Package__c pkg : [Select id, name, Latest_Version__c,Namespace__c from Package__c]) {
           obj01.listWrapper.add(new PackageController.WrapperClass(pkg));
        }
        obj01.allBool = true;
        obj01.selectAll();
        obj01.editAction();
        obj01.save();
    }
   }
       /*Test method to check select atlease one record for update*/
       static testMethod void test_save_UseCase4(){
         user currUser = [select Id,Name from User where Id=:UserInfo.getUserID()];
        system.runAs(currUser){
	PackageController obj01 = new PackageController();
    obj01.listWrapper = new List<PackageController.WrapperClass>();
   	for(Package__c pkg : [Select id, name, Latest_Version__c,Namespace__c from Package__c]) {
           obj01.listWrapper.add(new PackageController.WrapperClass(pkg));
        }
        obj01.allBool = false;
        obj01.selectAll();
    	obj01.editAction();
        obj01.save(); 
        }  
     }
    
    /*Test method for deleting records*/
     static testMethod void test_del_UseCase1(){
         user currUser = [select Id,Name from User where Id=:UserInfo.getUserID()];
        system.runAs(currUser){
	PackageController obj01 = new PackageController();
    obj01.listWrapper = new List<PackageController.WrapperClass>();
   	for(Package__c pkg : [Select id, name, Latest_Version__c,Namespace__c from Package__c]) {
           obj01.listWrapper.add(new PackageController.WrapperClass(pkg));
        }
        obj01.allBool = true;
        obj01.selectAll();
    	obj01.del();
            system.assertEquals(0, [SELECT count() FROM Package__c]);
        }  
     }
    
   /*Test method to check select atlease one record for delete*/
       static testMethod void test_del_UseCase2(){
         user currUser = [select Id,Name from User where Id=:UserInfo.getUserID()];
        system.runAs(currUser){
	PackageController obj01 = new PackageController();
    obj01.listWrapper = new List<PackageController.WrapperClass>();
   	for(Package__c pkg : [Select id, name, Latest_Version__c,Namespace__c from Package__c]) {
           obj01.listWrapper.add(new PackageController.WrapperClass(pkg));
        }
        obj01.allBool = false;
        obj01.selectAll();
    	obj01.del();  
        system.assertEquals(5, [SELECT count() FROM Package__c]);
        }  
     }
    
       /*Test method to check schema isDeleteable */
       static testMethod void test_del_UseCase3(){
       Profile p = [SELECT Id FROM Profile WHERE Name='Standard User' limit 1] ; //This profile don't have CRED permission for Package__C
        User usr = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                            EmailEncodingKey='UTF-8', FirstName='VTest',LastName='Testing', LanguageLocaleKey='en_US', 
                            LocaleSidKey='en_US', ProfileId = p.Id, TimeZoneSidKey='America/Los_Angeles', UserName='sssstandarduser@testorg.com');
        insert usr ;
        system.runAs(usr){
	PackageController obj01 = new PackageController();
    obj01.listWrapper = new List<PackageController.WrapperClass>();
   	for(Package__c pkg : [Select id, name, Latest_Version__c,Namespace__c from Package__c]) {
           obj01.listWrapper.add(new PackageController.WrapperClass(pkg));
        }
        obj01.allBool = true;
        obj01.selectAll();
    	obj01.del();  
        system.assertEquals(5, [SELECT count() FROM Package__c]);
        }  
     }
    
    /*Test method - callout*/
    static testMethod void test_loaddata_UseCase1(){
         PackageController obj01 = new PackageController();
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new HTTPMockCallout());
    	obj01.loaddata();
        Test.stopTest();
  }
        /*Test method - callout - schema isCreateable & isUpdateable*/
        static testMethod void test_loaddata_UseCase2(){
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User' limit 1] ; //This profile don't have CRED permission for Package__C
        User usr = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                            EmailEncodingKey='UTF-8', FirstName='VTest',LastName='Testing', LanguageLocaleKey='en_US', 
                            LocaleSidKey='en_US', ProfileId = p.Id, TimeZoneSidKey='America/Los_Angeles', UserName='sssstandarduser@testorg.com');
        insert usr ;
        system.runAs(usr){
         PackageController obj01 = new PackageController();
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new HTTPMockCallout());
    	obj01.loaddata();
        Test.stopTest();
        }
  }
    
}