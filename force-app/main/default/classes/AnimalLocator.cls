public class AnimalLocator {

    public static String getAnimalNameById (Integer id){
        Http http = new Http();
        String animalname ='';
        HttpRequest request = new HttpRequest();
        request.setEndpoint('https://th-apex-http-callout.herokuapp.com/animals/'+id);
        request.setMethod('GET');
        
        HttpResponse res = new HttpResponse();
        res = http.send(request);
        
        if(res.getStatusCode()==200){
            Map<String, Object> mapresult = (map<String, Object>) JSON.deserializeUnTyped(res.getBody());
            
           Map<String, Object> result = (Map<String, Object>) mapresult.get('animal');
            
            animalname = string.valueof(result.get('name'));

        }
        return animalname;
        
    }
    
}