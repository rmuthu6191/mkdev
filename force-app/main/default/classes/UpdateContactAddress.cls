/**
 * @description       : 
 * @author            : kumarmuthu9@gmail.com
 * @group             : 
 * @last modified on  : 08-08-2020
 * @last modified by  : kumarmuthu9@gmail.com
 * Modifications Log 
 * Ver   Date         Author                  Modification
 * 1.0   08-08-2020   kumarmuthu9@gmail.com   Initial Version
**/
global  class UpdateContactAddress implements Database.Batchable<SObject> {

    global Database.QueryLocator start(Database.BatchableContext bc ) {
        String query = 'SELECT ID, BillingCity, BillingCountry, (SELECT ID, MailingCity, MailingCountry FROM Contacts) FROM Account';
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext bc, List<Account> scope ) {
        
        List<contact> contacts = new List<contact>();
        for (Account account: scope) {   
            for (Contact contact : account.contacts ) {
                if(contact.MailingCity == null && contact.MailingCountry == null){
                    if( account.BillingCountry != null && account.BillingCity != null){
                    contact.MailingCountry = account.BillingCountry;
                    contact.MailingCity	= account.BillingCity;                
                    contacts.add(contact);
                    } else {
                        contact.MailingCountry = 'Not available';
                        contact.MailingCity = 'Not available';
                        contacts.add(contact);
                    }
                }
            }
        }
        if(contacts.size()>0){
            Database.update(contacts, false);
        }         
    }
    
    global void finish(Database.BatchableContext bc) {        
        
    }
    
}
