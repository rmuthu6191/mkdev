public class CasesSidebarController {
    
    public Map<String,Integer> caseCountByStatus {get; set;}
    
    public CasesSidebarController(){
        
        List<String> closedLabels = new List<String>();
        for(casestatus cand : [select MasterLabel from casestatus where IsClosed =true]){
            closedLabels.add(cand.MasterLabel);
        }        
        
        caseCountByStatus = new Map<String,Integer>();
        for(AggregateResult ar : [select status,count(id) caseCount from case where status NOT IN: closedLabels GROUP BY status]){
            caseCountByStatus.put((string)ar.get('status'), (Integer)ar.get('caseCount'));
        }
    }

}