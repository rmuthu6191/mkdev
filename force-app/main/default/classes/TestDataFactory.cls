@isTest
public class TestDataFactory {

    public static List<account> createAccountswithOppty (Integer numacc,Integer numoppty)
    {
        List<Account> AccLst = new List<Account>();
        for(integer i=0; i<numacc;i++){            
            Account Acc = new Account (name ='Test Data'+ i);
            AccLst.add(Acc);
        }
        
        insert AccLst;
        
        List<Opportunity> opps = new List<Opportunity> ();
        
        for(integer j=0;j<numacc;j++){
            
            Account AccOppty = AccLst[j];
            
            for(integer k=0;k<numoppty;k++){
                
                Opportunity opprec = new Opportunity (Name ='test data oppty'+k,StageName ='Value Proposition',closedate = system.today().addmonths(1),AccountID =AccOppty.id );
                opps.add(opprec);
            }

          
            
          
        }
          if(opps.size()>0)
            insert opps;
          return AccLst;    
    }
}