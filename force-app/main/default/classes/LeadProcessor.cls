global class LeadProcessor implements Database.Batchable<sObject> {

    global Database.QueryLocator start (Database.BatchableContext bc){
        String query = 'select id,LeadSource from lead';        
       return Database.getQueryLocator(query);
       
    }
    
    global void execute (Database.BatchableContext bc, List<lead> scope ){
	
        List<lead> leadres = new List<lead>();
        
        for(Lead leadup : scope){
            
            leadup.LeadSource ='Dreamforce';
            leadres.add(leadup);
        }
        
        update leadres;
    }
    
    global void finish (Database.BatchableContext bc){
        
    }
}