public class EditFromSearchController {
    Public List<Account> acc{get; set;}    
    Public EditFromSearchController(){
        Integer Idx =1;
        List<id> ids = new List<id>();
        String accStr;
        do{
            accStr = ApexPages.currentPage().getParameters().get('account'+Idx);
            if(accStr!=null){
                ids.add(accStr);
            }
            Idx++;
        }
        while(accStr!=null);
        if(ids.size()>0){
            acc = [select Name,Type, Industry from Account where id IN : ids];
        }
    }
    Public PageReference Save()    {
        update acc;
        return new PageReference('/001/o');
    }    
}