/**
 * @File Name          : HTTPMockCallout.cls
 * @Description        : 
 * @Author             : kumarmuthu9@gmail.com
 * @Group              : 
 * @Last Modified By   : kumarmuthu9@gmail.com
 * @Last Modified On   : 18/5/2020, 5:54:49 pm
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    18/5/2020   kumarmuthu9@gmail.com     Initial Version
**/
@isTest
global class HTTPMockCallout implements HttpCalloutMock {
    // global HTTPResponse respond(HTTPRequest req) {
        
    //     System.assertEquals('https://s3.amazonaws.com/targetx-sfdc-interview/packages.json', req.getEndpoint());
    //     System.assertEquals('GET', req.getMethod());
        
    //     HttpResponse response = new HttpResponse();
    //     response.setHeader('Content-Type', 'application/json');
    //     response.setBody('[{"Name":"Base","Namespace__c":"TX_Base","Latest_Version__c":1901.6},{"Name":"Recruitment Manager","Namespace__c":"TX_RM","Latest_Version__c":1902.4}]');
    //     response.setStatusCode(200);
    //     return response;
    // }

    global HTTPResponse respond(HTTPRequest req) {
        
        System.assertEquals('https://api.exchangeratesapi.io/latest?base=JPY', req.getEndpoint());
        System.assertEquals('GET', req.getMethod());
        HttpResponse response = new HttpResponse();
        response.setHeader('Content-Type', 'application/json');
        response.setBody('{"rates":{"CAD":0.0131835887,"HKD":0.0724426556,"ISK":1.3632822643,"PHP":0.4741452437,"DKK":0.0645511988,"HUF":3.0701982169,"CZK":0.2388037739,"GBP":0.0076809487,"RON":0.0418938804,"SEK":0.0923526357,"IDR":139.5985458323,"INR":0.7096728123,"BRL":0.0546801697,"RUB":0.6891785683,"HRK":0.0655180473,"JPY":1.0,"THB":0.2999740327,"CHF":0.0090998009,"EUR":0.0086557604,"MYR":0.0406664936,"BGN":0.0169289362,"TRY":0.0646490089,"CNY":0.0664407513,"NOK":0.0957050117,"NZD":0.0157058773,"ZAR":0.1737739115,"USD":0.0093464901,"MXN":0.224154765,"SGD":0.0133272743,"AUD":0.0145460054,"ILS":0.0330459621,"KRW":11.5346663204,"PLN":0.0395135463},"base":"JPY","date":"2020-05-15"}');
        response.setStatusCode(200);
        return response;
    }
}