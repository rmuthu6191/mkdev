public class OlderAccountsUtility {

    public static void updateOlderAccounts(){
        
        Account[] oldAccounts = [select id,description from Account ORDER BY CreatedDate ASC LIMIT 5];
        
        for (Account acct : oldAccounts){
            acct.description ='Heritage Account';
        }
        
        update oldAccounts;
    }
}